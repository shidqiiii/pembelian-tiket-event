<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//buath admin
Route::group(['middleware'=>'role:1'],function(){
    Route::get('/admin', function () {
        return view('admin.home');
    });

    Route::get('/user', 'UserController@index');
    Route::delete('/user', 'UserController@destroy');
    
    //CRUD Kategori
    Route::resource('kategori', 'KategoriController');
    //CRUD Event
    Route::resource('event', 'EventController');
});

Route::group(['middleware'=>'role:2'],function(){

});

Route::get('/show/{event_id}', 'UserController@showUser');
Route::get('/', 'UserController@indexUser');