@extends('admin.layout')
@section('title')
Halaman List User
@endsection
@section('content')

<table class="table">
    <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Nama</th>
            <th scope="col">Email</th>
            <th scope="col">Alamat</th>
            <th scope="col">Telefon</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($user as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{Str::limit($item->name, 10)}}</td>
            <td>{{Str::limit($item->email, 10)}}</td>
            <td>{{Str::limit($item->profile->alamat, 10)}}</td>
            <td>{{$item->profile->no_telepon}}</td>
            <td>
                <form action="/user/{{$item->id}}" method="POST" class="mt-2">
                    @csrf
                    @method('delete')
                    <input type="button" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <h1>Data tidak ditemukan</h1>
        @endforelse
    </tbody>
</table>
@endsection