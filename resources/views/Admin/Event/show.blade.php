@extends('admin.layout')
@section('title')
Halaman Event Detail
@endsection
@section('content')
<div class="card-body">
    <h4>Nama Event: {{$event->nama}}</h4>
    <h5>Lokasi Event: {{$event->lokasi}}</h5>
    <h5>Tanggal Event: {{ \Carbon\Carbon::parse($event->tanggal)->format('l, j F Y')}}</h5>
    <p>Deskripsi Event: {{$event->deskripsi}}</p>
    <img src="{{asset('images/'.$event->thumbnail)}}" class="img-fluid" alt=" " style="width: 30%"><br>
</div>
<a href="/event" class="btn btn-primary mt-3">Back</a>
@endsection