@extends('admin.layout')
@section('title')
Halaman Edit Event
@endsection
@section('content')
<form method="POST" action="/event/{{$event->id}}" enctype="multipart/form-data">
    @csrf
    @method("put")
    <div class="form-group">
        <label>Nama Event</label>
        <input type="text" name="nama" class="form-control" value="{{$event->nama}}">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea name="deskripsi" class="form-control" rows="5">{{$event->deskripsi}}</textarea>
    </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Lokasi</label>
        <input type="text" name="lokasi" class="form-control" value="{{$event->lokasi}}">
    </div>
    @error('lokasi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Tanggal</label>
        <input type="date" name="tanggal" class="form-control" value="{{$event->tanggal}}">
    </div>
    @error('tanggal')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Kategori</label>
        <select name="event_kategori_id" class="form-control">
            <option value="">---Pilih Kategori---</option>
            @foreach ($kategori as $item)
            @if ($item->id === $event->event_kategori_id)
            <option value="{{$item->id}}" selected>{{$item->kategori}}</option>
            @else
            <option value="{{$item->id}}">{{$item->kategori}}</option>
            @endif
            @endforeach
        </select>
    </div>
    @error('event_kategori_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" name="thumbnail" class="form-control">
    </div>
    @error('thumbnail')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection