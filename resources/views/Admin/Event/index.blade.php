@extends('admin.layout')
@section('title')
Halaman List Event
@endsection
@section('content')
<a href="/event/create" class="btn btn-primary">Tambah Event</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Nama Event</th>
            <th scope="col">Deskripsi Event</th>
            <th scope="col">Lokasi</th>
            <th scope="col">Tanggal</th>
            <th scope="col">Kategori Event</th>
            <th scope="col">Thumbnail</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($event as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{Str::limit($item->nama, 10)}}</td>
            <td>{{Str::limit($item->deskripsi, 10)}}</td>
            <td>{{Str::limit($item->lokasi, 10)}}</td>
            <td>{{$item->tanggal}}</td>
            <td>{{$item->kategori->kategori}}</td>
            <td class="col-1"><img src="{{asset('images/'.$item->thumbnail)}}" alt="" style="width: 40%"></td>
            <td>
                <form action="/event/{{$item->id}}" method="POST" class="mt-2">
                    <a href="/event/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/event/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="button" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <h1>Data tidak ditemukan</h1>
        @endforelse
    </tbody>
</table>
@endsection