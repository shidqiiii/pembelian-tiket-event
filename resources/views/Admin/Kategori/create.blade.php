@extends('admin.layout')
@section('title')
Halaman Tambah Kategori
@endsection
@section('content')
<form method="POST" action="/kategori">
    @csrf
    <div class="form-group">
        <label>Nama Kategori</label>
        <input type="text" name="kategori" class="form-control" placeholder="Masukan nama kategori">
    </div>
    @error('kategori')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection