@extends('admin.layout')
@section('title')
Halaman List Kategori
@endsection
@section('content')
<a href="/kategori/create" class="btn btn-primary">Tambah Kategori</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Jenis Event</th>
            <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->kategori}}</td>
            <td>
                <form action="/kategori/{{$item->id}}" method="POST" class="mt-2">
                    <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="button" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <h1>Data tidak ditemukan</h1>
        @endforelse
    </tbody>
</table>
@endsection