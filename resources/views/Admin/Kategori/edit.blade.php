@extends('admin.layout')
@section('title')
Halaman Edit Kategori
@endsection
@section('content')
<form method="POST" action="/kategori/{{$kategori->id}}">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama Kategori</label>
        <input type="text" name="kategori" class="form-control" value="{{$kategori->kategori}}"
            placeholder="Masukan nama kategori">
    </div>
    @error('kategori')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection