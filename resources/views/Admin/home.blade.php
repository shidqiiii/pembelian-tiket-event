@extends('admin.layout')
@section('title')
Halaman Admin
@endsection
@section('content')
<p>Selamat datang di halaman Admin</p>
<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <img class="card-img-top" style="height: 14rem;"
                src="https://static01.nyt.com/images/2017/07/20/fashion/20EVERYDAYPEOPLEweb6/20EVERYDAYPEOPLEweb6-jumbo.jpg"
                alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title" style="font-weight: bold">List User</h5>
                <p class="card-text">Berisi list user yang terdaftar di website</p>
                <a href="#" class="btn btn-primary">Go to User</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <img class="card-img-top" style="height: 14rem;"
                src="https://ecommercenews.eu/wp-content/uploads/2013/06/most_common_payment_methods_in_europe.png"
                alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title" style="font-weight: bold">List Transaksi</h5>
                <p class="card-text">Berisi list transaksi yang dilakukan oleh user</p>
                <a href="#" class="btn btn-primary">Go to Transaksi</a>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card">
            <img class="card-img-top" style="height: 14rem;"
                src="https://bigeventasia.id/wp-content/uploads/2019/11/event-concert.jpg?x81295" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title" style="font-weight: bold">List Event</h5>
                <p class="card-text">Berisi Event apa saja yang sedang berlangsung</p>
                <a href="/event" class="btn btn-primary">Go to Event</a>
            </div>
        </div>
    </div>
</div>
@endsection