<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <a class="navbar-brand" href="#">PesenTiket.com</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse ml-5" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/">Cari Event</a>
                </li>
                @auth
                @if(Auth::user()->status_user_id == 1)
                <li class="nav-item active">
                    <a class="nav-link" href="/admin">Go to Dashboard</a>
                </li>
                @endif
                <li class="nav-item active">
                    <a class="nav-link bg-danger text-white rounded" href="{{ route('logout') }}" onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                @endauth
                @guest
                <li class="nav-item active">
                    <a class="nav-link border border-white rounded" href="/login">Login</a>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>