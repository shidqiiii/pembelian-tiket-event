@extends('user.layout')
@section('content')
<!--Carousel-->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active" style="background-color: black;">
            <img src="https://img.jakpost.net/c/2020/09/29/2020_09_29_104990_1601342877._large.jpg"
                class="d-block w-100" alt="..." style="max-height: 20rem; object-fit: cover; opacity: 0.2;">
            <div class="carousel-caption d-none d-md-block">
                <h1>Nonton Konser ?</h1>
                <p>Beli aja di PesanTiket.com aja</p>
            </div>
        </div>
        <div class="carousel-item" style="background-color: black;">
            <img src="https://www.adobe.com/content/dam/cc/us/en/creativecloud/photography/discover/concert-photography/thumbnail.jpeg"
                class="d-block w-100" alt="..."
                style="max-height: 20rem; object-fit: cover; object-position: bottom; opacity: 0.2;">
            <div class="carousel-caption d-none d-md-block">
                <h1>Belum sempat beli tiketnya?</h1>
                <p>Pesan aja di PesanTiket.com aja</p>
            </div>
        </div>
        <div class="carousel-item" style="background-color: black;">
            <img src="https://www.halton.com/wp-content/uploads/2020/05/Concert_hall_rock_concert-1366x668.jpg"
                class="d-block w-100" alt="..."
                style="max-height: 20rem; object-fit: cover; object-position: bottom; opacity: 0.2;">
            <div class="carousel-caption d-none d-md-block">
                <h1>Takut tidak kebagian tiket ?</h1>
                <p>Beli aja di PesanTiket.com aja</p>
            </div>
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </button>
</div>
<!--End Carousel-->

<!--Card-->
<div class="container" style="margin-top: 5rem">
    <div class="row row-cols-1 row-cols-md-3">
        @forelse ($event as $item)
        <div class="col mb-4">
            <div class="card h-100">
                <img src="{{asset('images/'.$item->thumbnail)}}" class="card-img-top" alt="..."
                    style="max-height: 20rem; object-fit: cover;">
                <div class="card-body">
                    <h5 class="card-title">{{Str::limit($item->nama, 25)}}</h5>
                    <p class="card-text">{{Str::limit($item->deskripsi, 50)}}</p>
                    <a href="/show/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                </div>
                <div class="card-footer">
                    <span class="badge badge-pill badge-warning">{{$item->kategori->kategori}}</span>
                </div>
            </div>
        </div>
        @empty
        <h1>Data tidak ditemukan</h1>
        @endforelse
    </div>
</div>
<!--End Card-->
@endsection