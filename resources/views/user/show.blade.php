@extends('user.layout')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-horizontal" style="display: flex; flex: 1 1 auto;">
                    <div class="img-square-wrapper">
                        <img class="" src="{{asset('images/'.$event->thumbnail)}}" alt="..." style="max-width: 300px">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">{{$event->nama}}</h4>
                        <h6 class="card-text">{{$event->lokasi}}</h6>
                        <h6 class="card-text">{{ \Carbon\Carbon::parse($event->tanggal)->format('l, j F Y')}}</h6>
                        <p class="card-text">{{$event->deskripsi}}</p>
                        <a href="#" class="btn btn-primary btn-sm">Pesan Tiket</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection