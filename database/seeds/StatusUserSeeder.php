<?php

use Illuminate\Database\Seeder;

class StatusUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_user')->insert([
            ['status_user' => 'Admin'],
            ['status_user' => 'User']
        ]);
    }
}
