<?php

use Illuminate\Database\Seeder;

class StatusTransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_transaksi')->insert([
            ['status' => 'Belum Dibayar'],
            ['status' => 'Sudah Dibayar']
        ]);
    }
}
