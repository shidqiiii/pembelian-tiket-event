<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Albert',
            'email' => 'albert@gmail.com',
            'password' => Hash::make('12345'),
            'status_user_id' => '1'
        ]);

        DB::table('profile')->insert([
            'alamat' => 'Jakarta',
            'no_telepon' => '08121234123',
            'user_id' => '1'
        ]);
    }
}
