<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = "event";
    protected $fillable = ["nama", "deskripsi", "lokasi", "tanggal", "event_kategori_id", "thumbnail"];

    public function kategori(){
        return $this->belongsTo('App\Kategori', "event_kategori_id");
    }
}
