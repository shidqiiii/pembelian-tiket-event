<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    protected $table = "event_kategori";
    protected $fillable = ["kategori"];

    public function event(){
        return $this->hasOne('App\Event');
    }
}
