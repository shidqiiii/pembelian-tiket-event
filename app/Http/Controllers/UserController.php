<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\event;
use App\user;

class UserController extends Controller
{
    public function indexUser()
    {
        $event = Event::all();

        return view('user.index', compact('event'));
    }

    public function showUser($id)
    {
        $event = Event::find($id);

        return view('user.show', compact('event'));
    }

    public function index()
    {
        $user = User::all()->where('status_user_id', 2);

        return view('admin.user.index', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        
        $user->delete();
        return redirect('/user');

    }
}
