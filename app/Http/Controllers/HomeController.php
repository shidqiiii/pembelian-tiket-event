<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\UserController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $a = Auth::user()->status_user_id;
        // dd($a);
        if(Auth::user()->status_user_id === 1){
            return redirect('/admin');


        }
        if(Auth::user()->status_user_id === 2){
            return redirect()->action([UserController::class, 'indexUser']);
        }
    }
}
