<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\event;
use File;
use Auth;

class EventController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = Event::all();

        return view('admin.event.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('event_kategori')->get();
        return view('admin.event.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
            'lokasi' => 'required',
            'tanggal' => 'required',
            'event_kategori_id' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $thumbnailName = time().'.'.$request->thumbnail->extension();
        $request->thumbnail->move(public_path('images'), $thumbnailName);

        $event = new Event;

        $event->nama = $request->nama;
        $event->deskripsi = $request->deskripsi;
        $event->lokasi = $request->lokasi;
        $event->tanggal = $request->tanggal;

        $event->event_kategori_id = $request->event_kategori_id;
        $event->thumbnail = $thumbnailName;

        //dd($event);
        $event->save();

        return redirect('/event');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);

        return view('admin.event.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('event_kategori')->get();
        $event = Event::find($id);

        return view('admin.event.edit', compact('event', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
            'lokasi' => 'required',
            'tanggal' => 'required',
            'event_kategori_id' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $event = Event::find($id);
        
        if($request->has('thumbnail')){
            $thumbnailName = time().'.'.$request->thumbnail->extension();
            $request->thumbnail->move(public_path('images'), $thumbnailName);

            $event->nama = $request->nama;
            $event->deskripsi = $request->deskripsi;
            $event->lokasi = $request->lokasi;
            $event->tanggal = $request->tanggal;
            $event->event_kategori_id = $request->event_kategori_id;
            $event->thumbnail = $thumbnailName;
        }else{
            $event->nama = $request->nama;
            $event->deskripsi = $request->deskripsi;
            $event->lokasi = $request->lokasi;
            $event->tanggal = $request->tanggal;
            $event->event_kategori_id = $request->event_kategori_id;
        }

        $event->save();

        return redirect('/event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        $path = "images/";
        File::delete($path.$event->thumbnail);
        
        $event->delete();
        return redirect('/event');

    }
}
