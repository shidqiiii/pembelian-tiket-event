<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            // return redirect(RouteServiceProvider::HOME);
            $role = Auth::user()->status_user_id;

            switch ($role) {
                case '1':
                  return redirect('/admin');
                  break;
                case '2':
                  return redirect('/');
                  break; 
            
                default:
                  return redirect('/home'); 
                break;
              }
        }

        return $next($request);
    }
}
