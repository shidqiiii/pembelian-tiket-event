<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profile";
    protected $fillable = ["alamat", "no_telepon", "user_id"];

    public function user(){
        return $this->belongsTo('App\User', "user_id");
    }
}
